#include "uSynergy.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <swis.h>
#include <kernel.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdint.h>
#include <stdbool.h>
#include "keymap.h"

//vectors
#define KeyV     0x13
#define PointerV   38
//context m_clientname is this computer's client name.
//where is context coming from?
// context->m_clientName

//OS_Word 21,2 (SWI &07) Define mouse multipliers PRM1 730

//---------------------- Defines --------------------------------------------

#define HAL_CounterDelay 0x7A


typedef struct{
  unsigned char reason;
  //int16_t x;
  //int16_t y;

  //big to little endian. Better ways to do this.
  unsigned char xlsb;
  char xmsb;
  unsigned char ylsb;
  char ymsb; //
}mouseBlock;

typedef struct{
  unsigned int x;
  unsigned int y;
} point;


//---------------------- Globals --------------------------------------------

static point screenRes; //this is in pixels.
static point screenMult; //OS unit multiplier.

static char *uSynSrvName; //hmm
static int uSynergySocket;

//---------------------- Prototypes -----------------------------------------

void uSynSleep(uSynergyCookie cookie, int timeMs);

uSynergyBool uSynConnect( uSynergyCookie cookie);

uSynergyBool uSynSend( uSynergyCookie cookie, const uint8_t *buffer, int length );

uSynergyBool uSynReceive( uSynergyCookie cookie, uint8_t *buffer, int maxLength, int* outLength );

uint32_t uSynGetTime();

void uSynTrace( uSynergyCookie cookie, const char* text);

void uSynScrnActive( uSynergyCookie cookie, uSynergyBool active );

void uSynMouse(uSynergyCookie cookie, uint16_t x, uint16_t y, int16_t wheelX, int16_t wheelY, uSynergyBool buttonLeft, uSynergyBool buttonRight, uSynergyBool buttonMiddle);

void uSynKbd(uSynergyCookie cookie, uint16_t key, uint16_t modifiers, uSynergyBool down, uSynergyBool repeat);

void uSynJstk(uSynergyCookie cookie, uint8_t joyNum, uint16_t buttons, int8_t leftStickX, int8_t leftStickY, int8_t rightStickX, int8_t rightStickY);

void uSynClpBrd(uSynergyCookie cookie, enum uSynergyClipboardFormat format, const uint8_t *data, uint32_t size);
//------ my functions
void moveMouse(int16_t x, int16_t y);
point getResolution();

//---------------------- int main ------------------------------------------

int main(int argc, char* argv[]){
  _kernel_swi_regs reg;
  uSynergyContext context;
  uSynergyInit(&context);

  char wimpBlock[256];


  //set some defaults.
  //TODO: Make this configurable.
  context.m_clientName = "RISCOS";

  //argument parsing
/*
  -a = server ip address
  -n = name
  -c = client name visible to uSynergy PC
*/

  int opt; //is this needed? seems builtin.

  while(( opt = getopt( argc, argv, ":if:anc")) != -1)
  {
    switch( opt )
    {
      case 'a':
        printf( "Listen for address %s\n", optarg );
        //TODO: IP address sanitising and use.
        break;

      case 'n':
        break;

      case 'c':
        uSynSrvName = optarg;
        break;

    } //end switch( opt)

  }

 //end argument parsing


 //more hardwired stuff!
  screenRes = getResolution();
  /*
  OS_ScreenMode Reason code 1 gets resolution.

  */
  printf("Screen res is %d x %d\n", screenRes.x, screenRes.y );
  //screenRes.x = 1600;
  //screenRes.y = 1200;
  screenMult.x = 2; //needs to be set correctly per resolution.
  screenMult.y = 2;


  /*context.m_clientWidth=1600;
  context.m_clientHeight=1200;*/
  context.m_clientWidth = screenRes.x;
  context.m_clientHeight = screenRes.y;
  //lots of DINF messages still.

  //"Working" does _NOT_ mean finished.

  context.m_sleepFunc            = &uSynSleep;      //working
  context.m_connectFunc          = &uSynConnect;    //working
  context.m_sendFunc             = &uSynSend;       //working
  context.m_receiveFunc          = &uSynReceive;    //working
  context.m_getTimeFunc          = &uSynGetTime;    //working
  context.m_traceFunc            = &uSynTrace;      //stub
  context.m_screenActiveCallback = &uSynScrnActive; //stub
  context.m_mouseCallback        = &uSynMouse;      //working
  context.m_keyboardCallback     = &uSynKbd;        //working
  context.m_joystickCallback     = &uSynJstk;       //stub
  context.m_clipboardCallback    = &uSynClpBrd;     //stub

  //populate the keymap
  populateKeymap();

  for( ; ; ){
       uSynergyUpdate(&context);
/*
//How to make a program do weird things in one easy lesson.
       reg.r[0] = 0; //I have no idea what I am doing.
       reg.r[1] = (unsigned int) &wimpBlock;
       reg.r[2] = 20; //I don't know what unit!
       reg.r[3] = 0; //bit22 not set so irrelevent.

       _kernel_swi(Wimp_PollIdle, &reg, &reg);
  }
*/
//TODO: multitasking compliant loop.
       //TODO: exit with tidy up.
       }
    return 0;
}

//---------------------------------------------------========================

void uSynSleep(uSynergyCookie cookie, int timeMs){
/*  _kernel_swi_regs reg;
  timeMs *= 1000;
  reg.r[0] = timeMs;
  reg.r[8] = 0;
  reg.r[9] = 22;

  printf("sleep, timeMs=%d\n", timeMs);
  _kernel_swi(HAL_CounterDelay, &reg, &reg);*/
  //okay so something is wrong with the RISC OS way.
  //signed int perhaps?
  usleep(timeMs * 1000);

  //Probably shouldn't be using a HAL timer function.
  //hal_counter_delay(timeMs * 1000);
}
//---------------------------------------------------------------------------
uSynergyBool uSynConnect( uSynergyCookie cookie){
  printf("connect\n");
   // NOTE: You need to turn off "Use SSL Encryption" in Synergy preferences, since

    // uSynergy does not support SSL.



    struct addrinfo hints, *res;



    // first, load up address structs with getaddrinfo():

    memset(&hints, 0, sizeof hints);

    //does RO even support IPv6?
    hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever

    hints.ai_socktype = SOCK_STREAM;

    //----------HARD CODED TESTING JUNK--------------
    //uSynSrvName = "orangepipc";
    //uSynSrvName = "192.168.1.108"; //Opi PC
    //uSynSrvName = "192.168.1.103"; //RPi3

    //address of host running Synergy.
    //needs to be put in a file.
    // uSynSrvName = "192.168.1.100";
      uSynSrvName = "192.168.1.126";
/*
    if( !uSynSrvName )
      uSynSrvName = "127.0.0.1";
*/
    hints.ai_flags |= AI_NUMERICHOST;


    //------END HARD CODED TESTING JUNK--------------

    // get server address

   // getaddrinfo([g_serverName UTF8String], "24800", &hints, &res);
   //is this a nonstandard version^^^?

   //huh? server name, port number, hints, and ?
   getaddrinfo(uSynSrvName, "24800", &hints, &res);


    if (!res)

    {

        //NSLog( @"Could not find server: %@", g_serverName );
        printf( "Could not find server %s", uSynSrvName );

        return USYNERGY_FALSE;

    }



    // make a socket:
//    usynergy_sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

    uSynergySocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);



    // connect it to the address and port we passed in to getaddrinfo():

    int ret = connect(uSynergySocket, res->ai_addr, res->ai_addrlen);

    if (!ret) {

        //NSLog( @"Connect suceeded...");
        printf( "Connected successfully!\n" );

    } else {

        //NSLog( @"Connect failed, %d", ret );
      printf( "Could not connect.\n" );
      return USYNERGY_FALSE;
      //hopefully this will force an exit.
    }

  return USYNERGY_TRUE;
}

//---------------------------------------------------------------------------
uSynergyBool uSynSend( uSynergyCookie cookie, const uint8_t *buffer, int length ){
  //printf( "Sending\n" );
  send( uSynergySocket, buffer, length, 0 );
  return USYNERGY_TRUE;
}
//---------------------------------------------------------------------------
uSynergyBool uSynReceive( uSynergyCookie cookie, uint8_t *buffer, int maxLength, int* outLength ){
  *outLength = (int) recv( uSynergySocket, buffer, maxLength, 0 );
  //printf( "Receiving\n" );
  return USYNERGY_TRUE;
}
//---------------------------------------------------------------------------
uint32_t uSynGetTime(){
  //Large clock skew between server and clients causes issues.
      struct timeval  tv;

    gettimeofday(&tv, NULL);

  //printf( "getTime\n" );

    return (int32_t)((tv.tv_sec) * 1000 + (tv.tv_usec) / 1000);

  //printf( "getTime\n" );
  //return 0;
}
//---------------------------------------------------------------------------
void uSynTrace( uSynergyCookie cookie, const char* text){
  printf( "%s\n", text );
}
//---------------------------------------------------------------------------
void uSynScrnActive( uSynergyCookie cookie, uSynergyBool active ){
  //use this to disable a lot of the events when not active. TODO.
  if( active )
      printf("Screen is active\n");
  else
      printf("Screen is inactive\n");
}
//---------------------------------------------------------------------------
void uSynMouse(uSynergyCookie cookie, uint16_t x, uint16_t y, int16_t wheelX, int16_t wheelY, uSynergyBool buttonLeft, uSynergyBool buttonRight, uSynergyBool buttonMiddle){

  static bool lpressed, mpressed, rpressed, e1pressed, e2pressed;

  static int16_t wheelX_old, wheelY_old, wheelX_delta, wheelY_delta;

//  printf("mouse X=%d, Y=%d\n", x, y);
//Mouse is working now.

   _kernel_swi_regs reg;



//Scrollwheel attempt with PointerV 9
//*

  //works. Needs rollover code.
  if(wheelY != wheelY_old || wheelX != wheelX_old){
    wheelY_delta = wheelY_old - wheelY;
    wheelX_delta = wheelX_old - wheelX;
    reg.r[9] = PointerV;
    reg.r[0] = 9; //PointerV 9
    reg.r[1] = wheelY_delta;
    reg.r[2] = 0; //uSynergy doesn't support extra buttons.
    reg.r[3] = wheelX_delta;
    _kernel_swi(OS_CallAVector, &reg, &reg);
    wheelY_old = wheelY;
    wheelX_old = wheelX;
  }



/*//

/*
uSynergy doesn't seem to support 0x73 and 0x74 extra buttons.
Are those buttons for the mouse wheel maybe?
*/

   //Left
   if ( buttonLeft &! lpressed ){
     //was it just pressed?
     lpressed = true;
     reg.r[9] = KeyV;
     reg.r[0] = 2; //keydown
     reg.r[1] = 0x70; //LMB
     _kernel_swi(OS_CallAVector, &reg, &reg);

   }
   if ( !buttonLeft & lpressed){
     lpressed = false;
     reg.r[9] = KeyV;
     reg.r[0] = 1; //keyup
     reg.r[1] = 0x70;
     _kernel_swi(OS_CallAVector, &reg, &reg);
   }

   //Middle
   if ( buttonMiddle &! mpressed ){
     //was it just pressed?
     mpressed = true;
     reg.r[9] = KeyV;
     reg.r[0] = 2; //keydown
     reg.r[1] = 0x71; //LMB
     _kernel_swi(OS_CallAVector, &reg, &reg);

   }
   if ( !buttonMiddle & mpressed){
     mpressed = false;
     reg.r[9] = KeyV;
     reg.r[0] = 1; //keyup
     reg.r[1] = 0x71;
     _kernel_swi(OS_CallAVector, &reg, &reg);
   }

   //Right
   if ( buttonRight &! rpressed ){
     //was it just pressed?
     rpressed = true;
     reg.r[9] = KeyV;
     reg.r[0] = 2; //keydown
     reg.r[1] = 0x72; //LMB
     _kernel_swi(OS_CallAVector, &reg, &reg);

   }
   if ( !buttonRight & rpressed){
     rpressed = false;
     reg.r[9] = KeyV;
     reg.r[0] = 1; //keyup
     reg.r[1] = 0x72;
     _kernel_swi(OS_CallAVector, &reg, &reg);
   }
/*
  if( buttonLeft ){
    printf( "Select\n" );
    reg.r[0] = 0x99;
    reg.r[1] = 0x00;
    reg.r[2] = 0x70;
    _kernel_swi(OS_Byte, &reg, &reg);
    //0x70
  }
  if( buttonRight ){
    printf( "Menu\n" );
    reg.r[0] = 0x99;
    reg.r[1] = 0x00;
    reg.r[2] = 0x71;
    _kernel_swi(OS_Byte, &reg, &reg);
    //0x71
  }
  if( buttonMiddle ){
    printf( "adjust\n" );
    reg.r[0] = 0x99;
    reg.r[1] = 0x00;
    reg.r[2] = 0x72;
    _kernel_swi(OS_Byte, &reg, &reg);
    //0x72
  }
  */
  moveMouse((int16_t)x,(int16_t) y);//talk about foot shooting! FIXME
}
//---------------------------------------------------------------------------
void uSynKbd(uSynergyCookie cookie, uint16_t key, uint16_t modifiers, uSynergyBool down, uSynergyBool repeat){

  uint32_t ro_key;
  uint32_t key_state;
  _kernel_swi_regs reg;
//  printf("keypress key %d, down %d, repeat %d\n", key, down, repeat);
  printf("keypress key 0x%X, down %d, repeat %d\n", key, down, repeat);
//following line causing issues because of special chars.
//  printf("Character is %c\n", key);
  ro_key = ro_keymap[key];
  if(down){
    key_state = 2;
  }
  else{
    key_state = 1;
  }

  reg.r[9] = KeyV;
  reg.r[0] = key_state;
  reg.r[1] = ro_key;

  _kernel_swi(OS_CallAVector, &reg, &reg);
  /*

  The keycodes received are output by xev

  OS_Byte 138 (0x8A) Insert byute into buffer

  The code below will not function correctly.
  It is just a reminder to try using the following as a basis of key input.
  */

/*keycode isn'y what I expected. Disabled for now for fixing*/
  /*
  _kernel_swi_regs reg;
  reg.r[0] = 0x99;
  reg.r[1] = 0x00; //keyboard buffer
  reg.r[2] = key;
  _kernel_swi(OS_Byte, &reg, &reg);
  */
}
//---------------------------------------------------------------------------
void uSynJstk(uSynergyCookie cookie, uint8_t joyNum, uint16_t buttons, int8_t leftStickX, int8_t leftStickY, int8_t rightStickX, int8_t rightStickY)
{
	printf("joystick, left=%d,%d right=%d,%d\n", leftStickX, leftStickY,
		rightStickX, rightStickY);
}
//---------------------------------------------------------------------------
void uSynClpBrd(uSynergyCookie cookie, enum uSynergyClipboardFormat format, const uint8_t *data, uint32_t size)
{
	printf("clipboard, size=%d\n", size);
}
//---------------------------------------------------------------------------
void moveMouse(int16_t x, int16_t y){
  //Why the hell do I have to do this garbage???
  /*
  *Y coordinate is opposite that is sent.

  *Coordinates are not in pixels. They are in RISC OS units.
  They need to be converted.

  */

  //let's turn Y upside down!
  y = screenRes.y - y;

  x *= screenMult.x;
  y *= screenMult.y;

//still need to convert to RISC OS units
  _kernel_swi_regs reg;
  mouseBlock m_mB;

  m_mB.xlsb = x & 0xff;//unsigned
  m_mB.xmsb = (char)(x>>8);//signed
  m_mB.ylsb = y & 0xff; //unsigned
  m_mB.ymsb = (char)(y>>8);
  m_mB.reason = 0x3;
  reg.r[0] = 0x15; //set mouse position.
  reg.r[1]=(unsigned int)&m_mB;

  _kernel_swi(OS_Word, &reg, &reg);

  m_mB.reason = 0x5;
  _kernel_swi(OS_Word, &reg, &reg);

}
//---------------------------------------------------------------------------
point getResolution(){
  point res;
  res.x = 0;
  res.y = 0;
  int resX = 0;
  int resY = 0;
  _kernel_swi_regs reg;
  reg.r[0] = -1;
  reg.r[1] = 11; //x res.
  _kernel_swi(OS_ReadModeVariable, &reg, &reg);
  res.x = reg.r[2] + 1;
  //printf("X resolution is: %d\n", resX);
  //reg.r[0] = -1;
  reg.r[1] = 12; //y res.
  _kernel_swi(OS_ReadModeVariable, &reg, &reg);
  res.y = reg.r[2] + 1;
  //printf("Y resolution is: %d\n", resY);
  return res;

}
